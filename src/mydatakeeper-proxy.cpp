#include <filesystem>
#include <fstream>
#include <iostream>

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>
#include <mydatakeeper/systemd.h>

using namespace std;

// Child PID
pid_t pid;

DBus::BusDispatcher dispatcher;

/* Squid configuration files */
string config_file;
string whitelist_file;
string ecap_file;
string headers_file;
string http_file;

/* Lease file */
string lease_file;

void setup()
{
    clog << "Setting up proxy" << endl;

    string err;
    if (execute("/usr/lib/mydatakeeper-proxy/iptables-update", {"add"}, nullptr, &err) != 0) {
        cerr << "/usr/lib/mydatakeeper-proxy/iptables-update: " << err << endl;
        exit(EXIT_FAILURE);
    }
    if (execute("/usr/lib/mydatakeeper-proxy/pac-update", {"add"}, nullptr, &err) != 0) {
        cerr << "/usr/lib/mydatakeeper-proxy/pac-update: " << err << endl;
        exit(EXIT_FAILURE);
    }
}

void teardown()
{
    clog << "Tearing down proxy" << endl;

    string err;
    if (execute("/usr/lib/mydatakeeper-proxy/iptables-update", {"remove"}, nullptr, &err) != 0) {
        cerr << "/usr/lib/mydatakeeper-proxy/iptables-update: " << err << endl;
        exit(EXIT_FAILURE);
    }
    if (execute("/usr/lib/mydatakeeper-proxy/pac-update", {"remove"}, nullptr, &err) != 0) {
        cerr << "/usr/lib/mydatakeeper-proxy/pac-update: " << err << endl;
        exit(EXIT_FAILURE);
    }
}

void createSwap()
{
    clog << "Creating swap directories" << endl;

    string err;
    const vector<string> args = {
        "--foreground",
        "-z",
        "-f",
        config_file
    };
    if (execute("/usr/sbin/squid", args, nullptr, &err) != 0) {
        cerr << "/usr/sbin/squid: " << err << endl;
        exit(EXIT_FAILURE);
    }

}

void forkOrReload()
{
    if (pid == 0) {
        createSwap();
        clog << "Forking and launching squid process" << endl;
        if ((pid = fork()) == 0) {
            const char *args[] = {
                "/usr/sbin/squid",
                "--foreground",
                "-sYC",
                "-f",
                config_file.c_str(),
                (char *)0
            };
            execv(args[0], (char* const*)args);
            // This process is not meant to be exited ever
            perror("execv");
            exit(EXIT_FAILURE);
        } else {
            setup();
        }
    } else {
        teardown();
        clog << "Reloading squid process configuration" << endl;
        if (kill(pid, SIGHUP) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
        setup();
    }
}

void killIfForked()
{
    if (pid != 0) {
        clog << "Killing openvpn process" << endl;
        if (kill(pid, SIGTERM) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
        int status;
        if (waitpid(pid, &status, WUNTRACED | WCONTINUED) == -1) {
            perror("wait");
            exit(EXIT_FAILURE);
        }
        pid = 0;
        teardown();
    } else {
        clog << "No openvpn process to kill" << endl;
    }
}

int generateWhitelist(auto& squid)
{
    clog << "Writing whitelist file" << endl;
    ofstream ofs (whitelist_file, ofstream::out | ofstream::trunc);

    /* Define a whitelist of domain */
    ofs << "include /etc/squid/squid.whitelist.banks.conf" << endl;

    ofs.close();

    return 0;
}

int generateHttp(auto& squid)
{
    bool https_proxy = squid.Get(fr::mydatakeeper::ConfigInterface, "https_proxy");
    bool transparent_http_proxy = squid.Get(fr::mydatakeeper::ConfigInterface, "transparent_http_proxy");
    bool transparent_https_proxy = squid.Get(fr::mydatakeeper::ConfigInterface, "transparent_https_proxy");

    clog << "Writing HTTP file" << endl;
    ofstream ofs (http_file, ofstream::out | ofstream::trunc);

    /* Deny requests to certain unsafe ports */
    ofs << "acl Safe_ports port 21" << endl; // FTP
    ofs << "acl Safe_ports port 70" << endl; // Gopher
    ofs << "acl Safe_ports port 80" << endl; // HTTP
    ofs << "acl Safe_ports port 210" << endl; // WAIS
    ofs << "acl Safe_ports port 280" << endl; // HTTP-MGMT
    ofs << "acl Safe_ports port 443" << endl; // HTTPS
    ofs << "acl Safe_ports port 488" << endl; // GSS-HTTP
    ofs << "acl Safe_ports port 591" << endl; // FileMaker
    ofs << "acl Safe_ports port 777" << endl; // Multiling HTTP
    ofs << "acl Safe_ports port 1025-65535" << endl; // Registered ports
    ofs << "http_access deny !Safe_ports" << endl;

    /* Deny CONNECT when proto is set and path is not empty */
    ofs << "acl CONNECT_method method CONNECT" << endl;
    ofs << "acl CONNECT_proto proto NONE" << endl;
    ofs << "acl CONNECT_path urlpath_regex ^$" << endl;
    ofs << "http_access deny CONNECT_method !CONNECT_proto" << endl;
    // Ugly hack : CONNECT_path = -1 which is converted to !CONNECT_path = 1
    //             because path is empty. So we match both forms
    ofs << "http_access deny CONNECT_method !CONNECT_path CONNECT_path" << endl;

    /* Only allow cachemgr access from localhost */
    ofs << "http_access allow localhost manager" << endl;
    ofs << "http_access deny manager" << endl;

    /* We strongly recommend the following be uncommented to protect innocent */
    /* web applications running on the proxy server who think the only */
    /* one who can access services on "localhost" is a local user */
    /* ofs << "http_access deny to_localhost" << endl; */

    /* Allow access from local networks as defined by IANA */
    ofs << "acl localnet src 0.0.0.0/8" << endl;
    ofs << "acl localnet src 10.0.0.0/8" << endl;
    ofs << "acl localnet src 100.64.0.0/10" << endl;
    ofs << "acl localnet src 169.254.0.0/16" << endl;
    ofs << "acl localnet src 172.16.0.0/12" << endl;
    ofs << "acl localnet src 192.168.0.0/16" << endl;
    ofs << "acl localnet src fc00::/7" << endl;
    ofs << "acl localnet src fe80::/10" << endl;
    ofs << "http_access allow localnet" << endl;
    ofs << "http_access allow localhost" << endl;

    /* And finally deny all other access to this proxy */
    ofs << "http_access deny all" << endl;

    /* HTTP proxy on port 3128 (default) */
    ofs << "http_port 3128" << endl;

    if (https_proxy) {
        /* HTTPS proxy on port 3129 */
        ofs << "http_port 3129"
            << " ssl-bump"
            <<  " generate-host-certificates=on"
            <<  " dynamic_cert_mem_cache_size=4MB"
            <<  " tls-cert=/var/lib/squid/ssl_cert/cert.pem"
            <<  " tls-key=/var/lib/squid/ssl_cert/cert.key" << endl;
    }

    if (transparent_http_proxy) {
        /* HTTP transparent proxy on port 3130 */
        ofs << "http_port 3130 intercept" << endl;
    }

    if (transparent_https_proxy) {
        /* HTTPS transparent proxy on port 3131 */
        ofs << "https_port 3131 intercept"
            << " ssl-bump"
            <<  " generate-host-certificates=on"
            <<  " dynamic_cert_mem_cache_size=4MB"
            <<  " tls-cert=/var/lib/squid/ssl_cert/cert.pem"
            <<  " tls-key=/var/lib/squid/ssl_cert/cert.key" << endl;
    }

    if (https_proxy || transparent_https_proxy) {
        /* SSL/TLS bump steps */
        ofs << "acl step1 at_step SslBump1" << endl;
        ofs << "acl step2 at_step SslBump2" << endl;
        ofs << "acl step3 at_step SslBump3" << endl;

        /* SSL/TLS bump parameters */
        ofs << "ssl_bump splice whitelist" << endl;
        ofs << "ssl_bump peek step1 all" << endl;
        ofs << "ssl_bump bump all" << endl;

        /* List of local domains */
        ofs << "acl local ssl::server_name .mydatakeeper.local" << endl;
        ofs << "acl local ssl::server_name .local.mydatakeeper.fr" << endl;
        ofs << "acl local ssl::server_name .home.mydatakeeper.fr" << endl;

        /* SSL/TLS proxy parameters */
        ofs << "sslproxy_cert_sign signTrusted local" << endl;
        ofs << "sslproxy_cert_error allow all" << endl;

        /* SSL/TLS parameters */
        ofs << "tls_outgoing_options"
            << " cipher=ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES128-GCM-SHA256:!aNULL:!eNULL:!LOW:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS:!RC4"
            << " min-version=1.2"
            << " options=NO_SSLv3,NO_TLSv1,NO_TLSv1_1,CIPHER_SERVER_PREFERENCE,No_Compression,NO_TICKET" << endl;

        /* SSL/TLS certificate generator parameters */
        ofs << "sslcrtd_program"
            << " /usr/lib/squid/security_file_certgen -s /var/lib/squid/ssl_cert -M 4MB" << endl;
        ofs << "sslcrtd_children 4 startup=1 idle=1" << endl;
    }


    ofs.close();

    return 0;
}

int generateHeaders(auto& squid)
{
    string protection_level = squid.Get(fr::mydatakeeper::ConfigInterface, "protection_level");

    clog << "Writing Headers file" << endl;
    ofstream ofs (headers_file, ofstream::out | ofstream::trunc);

    /* HTTP headers */
    /*
     * List of most common HTTP headers :
     * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
     */

    /*
     * Lists of headers blocked for http anonymization :
     * http://www.squid-cache.org/Doc/config/request_header_access/
     * http://www.squid-cache.org/Doc/config/reply_header_access/
     *
     * WARNING: Doing this VIOLATES the HTTP standard.  Enabling
     * this feature could make you liable for problems which it
     * causes.
     */



    if (protection_level == "standard") { /* http_anonymizer standard */
        ofs << "acl tproxy myport 3130" << endl;
        ofs << "acl tproxy myport 3131" << endl;

        ofs << "request_header_access Forwarded-For deny tproxy" << endl;
        ofs << "request_header_access From deny all" << endl;
        ofs << "request_header_access Referer deny all" << endl;
        ofs << "request_header_access Via deny tproxy" << endl;
        ofs << "request_header_access X-Forwarded-For deny tproxy" << endl;

        ofs << "reply_header_access Link deny all" << endl;
        ofs << "reply_header_access Server deny all" << endl;
        ofs << "request_header_access Via deny tproxy" << endl;
        ofs << "reply_header_access WWW-Authenticate deny all" << endl;
    } else if (protection_level == "advanced") { /* http_anonymizer advanced */
        ofs << "request_header_access Forwarded-For deny all" << endl;
        ofs << "request_header_access From deny all" << endl;
        ofs << "request_header_access Referer deny all" << endl;
        ofs << "request_header_access User-Agent deny all" << endl;
        ofs << "request_header_access Via deny all" << endl;
        ofs << "request_header_access X-Forwarded-For deny all" << endl;

        ofs << "reply_header_access Link deny all" << endl;
        ofs << "reply_header_access Server deny all" << endl;
        ofs << "reply_header_access Via deny all" << endl;
        ofs << "reply_header_access WWW-Authenticate deny all" << endl;
    } else if (protection_level == "paranoid") { /* http_anonymizer paranoid */
        ofs << "request_header_access Accept allow all" << endl;
        ofs << "request_header_access Accept-Charset allow all" << endl;
        ofs << "request_header_access Accept-Encoding allow all" << endl;
        ofs << "request_header_access Accept-Language allow all" << endl;
        ofs << "request_header_access All deny all" << endl;
        ofs << "request_header_access Authorization allow all" << endl;
        ofs << "request_header_access Cache-Control allow all" << endl;
        ofs << "request_header_access Connection allow all" << endl;
        ofs << "request_header_access Content-Length allow all" << endl;
        ofs << "request_header_access Content-Type allow all" << endl;
        ofs << "request_header_access Date allow all" << endl;
        ofs << "request_header_access Host allow all" << endl;
        ofs << "request_header_access If-Modified-Since allow all" << endl;
        ofs << "request_header_access Pragma allow all" << endl;
        ofs << "request_header_access Proxy-Authorization allow all" << endl;

        ofs << "reply_header_access All deny all" << endl;
        ofs << "reply_header_access Allow allow all" << endl;
        ofs << "reply_header_access Cache-Control allow all" << endl;
        ofs << "reply_header_access Connection allow all" << endl;
        ofs << "reply_header_access Content-Disposition allow all" << endl;
        ofs << "reply_header_access Content-Encoding allow all" << endl;
        ofs << "reply_header_access Content-Language allow all" << endl;
        ofs << "reply_header_access Content-Length allow all" << endl;
        ofs << "reply_header_access Content-Type allow all" << endl;
        ofs << "reply_header_access Date allow all" << endl;
        ofs << "reply_header_access Expires allow all" << endl;
        ofs << "reply_header_access Last-Modified allow all" << endl;
        ofs << "reply_header_access Location allow all" << endl;
        ofs << "reply_header_access Pragma allow all" << endl;
        ofs << "reply_header_access Proxy-Authenticate allow all" << endl;
        ofs << "reply_header_access Retry-After allow all" << endl;
        ofs << "reply_header_access Title allow all" << endl;
        ofs << "reply_header_access WWW-Authenticate allow all" << endl;
    }

    ofs.close();
    return 0;
}

int generateEcap(auto& squid)
{
    // Perform all the "risky" DBus calls before openning files
    // to avoid dangling open files descriptors
    bool ad_block_enabled = squid.Get(fr::mydatakeeper::ConfigInterface, "ad_block_enabled");
    bool tracking_protection_enabled = squid.Get(fr::mydatakeeper::ConfigInterface, "tracking_protection_enabled");
    bool wrap_javascript = squid.Get(fr::mydatakeeper::ConfigInterface, "wrap_javascript");
    bool mydatakeeper_menu = squid.Get(fr::mydatakeeper::ConfigInterface, "mydatakeeper_menu");
    bool captive_portal = squid.Get(fr::mydatakeeper::ConfigInterface, "captive_portal");

    // TODO: list ad-blocking/tracking-protection lists + stats

    clog << "Writing eCap file" << endl;
    ofstream ofs (ecap_file, ofstream::out | ofstream::trunc);

    /* eCap settings */
    ofs << "ecap_enable on" << endl;

    /* Load MDK eCap Modifier module */
    ofs << "loadable_modules /usr/lib/squid/libmydatakeeper-ecap.so" << endl;

    /* Provide source IP address */
    ofs << "adaptation_send_client_ip on" << endl;

    /* MDK eCap request Modifier service */
    if (ad_block_enabled || tracking_protection_enabled || captive_portal) {
        ofs << "ecap_service mdkRequestFilter reqmod_precache";
        if (captive_portal)
            ofs << " leasefile=" << lease_file;
        ofs << " uri=ecap://mydatakeeper.fr/ecap/services/modifier?mode=reqmod" << endl;
        ofs << "adaptation_access mdkRequestFilter allow !whitelist !CONNECT_method" << endl;
    }

    /* MDK eCap response Modifier service */
    if (wrap_javascript || mydatakeeper_menu) {
        ofs << "ecap_service mdkResponseModifier respmod_precache";
        if (wrap_javascript)
            ofs << " script_starttag_after_file=/usr/share/mydatakeeper-proxy/script_prepend.js"
                << " script_endtag_before_file=/usr/share/mydatakeeper-proxy/script_append.js";
        if (mydatakeeper_menu)
            ofs << " body_starttag_after_file=/usr/share/mydatakeeper-proxy/body_prepend.html";
        ofs << " uri=ecap://mydatakeeper.fr/ecap/services/modifier?mode=respmod" << endl;
        ofs << "adaptation_access mdkResponseModifier allow !whitelist !CONNECT_method" << endl;
    }

    ofs.close();

    return 0;
}

int generateConfig(auto& squid)
{
    generateWhitelist(squid);
    generateHttp(squid);
    generateHeaders(squid);
    generateEcap(squid);

    clog << "Writing config.conf file" << endl;
    ofstream ofs (config_file, ofstream::out | ofstream::trunc);

    /* Use localhost as dns server */
    ofs << "dns_nameservers 127.0.0.1" << endl;

    /* Log files */
    ofs << "cache_log /var/log/squid/cache.log" << endl;
    ofs << "access_log /var/log/squid/access.log" << endl;

    /* PID file */
    ofs << "pid_filename /run/squid/squid.pid" << endl;

    /* Coredumps directory */
    ofs << "coredump_dir /var/cache/squid" << endl;

    /* Cache settings */
    ofs << "cache_mem 4 MB" << endl;
    ofs << "cache_dir ufs /var/cache/squid 100 16 256" << endl;

    /* Memory pools setings */
    ofs << "memory_pools off" << endl;

    /* Default cache refresh pattern */
    ofs << "refresh_pattern    ^ftp:           1440 20% 10080" << endl;
    ofs << "refresh_pattern    ^gopher:        1440 0%  1440" << endl;
    ofs << "refresh_pattern -i (/cgi-bin/|\\?) 0    0%  0" << endl;
    ofs << "refresh_pattern    .               0    20% 4320" << endl;

    /* Include files */
    ofs << "include " << whitelist_file << endl;
    ofs << "include " << http_file << endl;
    ofs << "include " << headers_file << endl;
    ofs << "include " << ecap_file << endl;

    ofs.close();

    return 0;
}

inline void update(auto& squid, auto& unit)
{
    try {
        if (generateConfig(squid) == 0) {
            forkOrReload();
        }
    } catch (DBus::Error& e) {
        cerr << "An error occured during Squid config update : " << e.message() << endl;
    }
}

void handler(int sig)
{
    clog << "Stopping DBus main loop (sig " << sig << ')' << endl;
    dispatcher.leave();
    killIfForked();
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, unit, conf_dir;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/app/proxy", nullptr,
            "The Dbus path which handles the configuration"
        },
        {
            unit, "unit", 0, mydatakeeper::required_argument, "fr.mydatakeeper.app.proxy.service", nullptr,
            "The name of the unit to handle"
        },
        {
            conf_dir, "conf-dir", 0, mydatakeeper::required_argument, "/var/lib/mydatakeeper-config/apps/fr.mydatakeeper.app.proxy", nullptr,
            "The folder where configuration files will be generated"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    // Squid config files
    config_file = conf_dir + "/squid.conf";
    whitelist_file = conf_dir + "/squid.whitelist.conf";
    ecap_file = conf_dir + "/squid.ecap.conf";
    headers_file = conf_dir + "/squid.headers.conf";
    http_file = conf_dir + "/squid.http.conf";

    // Lease file
    lease_file = conf_dir + "/leases.txt";

    clog << "Getting Systemd proxy" << endl;
    org::freedesktop::systemd1::ManagerProxy systemd(conn);
    string unit_path = systemd.LoadUnit(unit);

    clog << "Getting Unit " << unit_path << " proxy" << endl;
    org::freedesktop::systemd1::UnitProxy squid(conn, unit_path);

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy proxy(conn, bus_path, bus_name);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    proxy.onPropertiesChanged = [&proxy, &squid](auto& iface, auto&changed, auto& invalidated)
    {
        clog << "Proxy config has changed" << endl;
        update(proxy, squid);
    };

    // Force update during init
    update(proxy, squid);

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}