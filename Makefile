# Makefile
.SUFFIXES:

CXX:=$(CROSS_COMPILE)g++
EXTRA_CFLAGS:=-std=c++17 -fconcepts -I./include $(shell pkg-config mydatakeeper --cflags)
EXTRA_LDFLAGS:=$(shell pkg-config mydatakeeper --libs) -lstdc++fs

TARGET=mydatakeeper-proxy
OBJ_FILES=mydatakeeper-proxy.o

all: $(TARGET)

$(TARGET): $(OBJ_FILES)
	$(CXX) $^ -o $@ $(LDFLAGS) $(EXTRA_LDFLAGS)

%.o:%.cpp
	$(CXX) -c $< -o $@ $(CFLAGS) $(EXTRA_CFLAGS)

clean:
	rm -rf $(TARGET) $(OBJ_FILES)
